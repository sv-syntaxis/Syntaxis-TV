import 'package:flutter/material.dart';
import 'package:syntaxis_tv/ui/info_feed.dart';
import 'package:syntaxis_tv/ui/poster_slider.dart';
import 'ui/partner_row.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Syntaxis TV'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title,}) : super(key: key);
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  @override
  Widget build(BuildContext context) {
    return Row(
      children: const [
        Flexible( flex: 3, child: PartnerRow()),
        Flexible( flex: 5, child: PosterSlider()),
        Flexible( flex: 3, child: InfoFeed()),
      ],
    );
  }
}
